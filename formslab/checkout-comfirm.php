<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Checkout example · Bootstrap</title>

    

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <!-- Custom styles for this template -->
    <link href="form-validation.css" rel="stylesheet">
  </head>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img class="d-block mx-auto mb-4" src="bootstrap-solid.svg" alt="" width="72" height="72">
    <h2>Checkout form</h2>
    <p class="lead">Below is an example form built entirely with Bootstrap’s form controls. Each required form group has a validation state that can be triggered by attempting to submit the form without completing it.</p>
  </div>
 </html>


<?php
echo "<h3>Name and Address:</h4>";
$firstname = $_GET['firstname'];
echo "<b>Firstname:</b> ". $firstname;
echo "<br>";

$lastname = $_GET['lastname'];
echo "<b>Lastname: </b>". $lastname;
echo "<br>";

$email = $_GET['email'];
echo "<b>Email:</b> ". $email;
echo "<br>";

$address = $_GET['address'];
echo "<b>Address: </b>". $address;
echo "<br>";

$address2 = $_GET['address2'];
if($address2 != ""){
    echo "<b>Address2: </b>".$address2;
}
echo "<br>";
echo "<h4>Payment information:</h4>";
$country = $_GET['country'];
echo "<b>Country: </b>".$country[0];
echo "<br>";

$provience = $_GET['provience'];
echo "<b>Provience: </b>".$provience[0];
echo "<br>";

$zip = $_GET['zip'];
echo "<b>Zip: </b>".$zip;
echo "<br>";

$gift = $_GET['gift'];
echo "<b>Giftwrap: </b>".$gift[0];
echo "<br>";

$d_method = $_GET['d_method'];
echo "<b>Delivery Method: </b>".$d_method[0];
echo "<br>";

$c_name = $_GET['c_name'];
echo "<b>Card Holder: </b>".$c_name;
echo "<br>";

$c_num = $_GET['c_num'];
echo "<b>Card Number: </b>".$c_num;
echo "<br>";

$c_exp = $_GET['c_exp'];
echo "<b>Card expire date: </b>".$c_exp;
echo "<br>";

$c_cvv = $_GET['c_cvv'];
echo "<b>Card cvv: </b>".$c_cvv;
echo "<br>";
echo "<br>";
echo "<h4>Quantity:</h4>";
$num_first = $_GET['num_first'];
echo "<b>Quantity of first product($12):</b>" .$num_first;
echo "<br>";

$num_second = $_GET['num_second'];
echo "<b>Quantity of second product($8):</b>" .$num_second;
echo "<br>";

$num_third = $_GET['num_third'];
echo "<b>Quantity of third product($5):</b>" .$num_third;
echo "<br>";

$fir_item_cost = 12*$num_first;
$sec_item_cost = 8*$num_second;
$third_item_cost = 5*$num_third;

echo "<br>";
echo "<h4>Cost:</h4>";
$sumb4tax = 0.00;
if($num_first !="" || $num_second !="" || $num_third !=""){
    $sumb4tax += $fir_item_cost + $sec_item_cost + $third_item_cost;
    if($gift[0] == "True +$1.00"){
        $sumb4tax +=1.00;
        if($d_method[0] == "Mail +$2.00"){
            $sumb4tax +=2.00;
        }else if($d_method[0] == "Courtier +$3.00"){
            $sumb4tax +=3.00;
        }
    }
}echo "<b>Total before tax: </b>$" .$sumb4tax;
echo "<br>";
$total = 0.00;
$tax = 0.00;
if ($provience[0] == "Alberta"){
    $tax = $sumb4tax * 0.05;
    echo "<b>Alberta(5%): </b>+".$tax;
}else if ($provience[0] == "British Columbia(BC)"){
    $tax = $sumb4tax * 0.12;
    echo "<b>British Columbia(12%): </b>+".$tax;
}else if ($provience[0] == "Manitoba"){
    $tax = $sumb4tax * 0.13;
    echo "<b>Manitoba(13%): </b>+".$tax;
}else if ($provience[0] == "New-Brunsiwick"){
    $tax = $sumb4tax * 0.15;
    echo "<b>New-Brunsiwick(15%): </b>+$".$tax;
}else if ($provience[0] == "Newfoundland and Labrador"){
    $tax = $sumb4tax * 0.15;
    echo "<b>Newfoundland and Labrador(15%): </b>+$".$tax;
}else if ($provience[0] == "Northwest Territories"){
    $tax = $sumb4tax * 0.05;
    echo "<b>Northwest Territories(5%): </b>+$".$tax;
}else if ($provience[0] == "Nova Scotia"){
    $tax = $sumb4tax * 0.15;
    echo "<b>Nova Scotia(15%): </b>+$".$tax;
}else if ($provience[0] == "Nunavut"){
    $tax = $sumb4tax * 0.05;
    echo "<b>Nunavut(5%): </b>+$".$tax;
}else if ($provience[0] == "Ontario"){
    $tax = $sumb4tax * 0.13;
    echo "<b>Ontario(13%): </b>+$".$tax;
}else if ($provience[0] == "Prince Edward Island(PEI)"){
    $tax = $sumb4tax * 0.15;
    echo "<b>Prince Edward Island(15%):</b> +$".$tax;
}else if ($provience[0] == "Quebec"){
    $tax = $sumb4tax * 0.14975;
    echo "<b>Quebec(14.975%):</b> +$".$tax;
}else if ($provience[0] == "Saskatchewan"){
    $tax = $sumb4tax * 0.11;
    echo "<b>Saskatchewan(11%):</b> +$".$tax;
}else if ($provience[0] == "Yukon"){
    $tax = $sumb4tax * 0.05;
    echo "<b>Yukon(5%): </b>+$".$tax;
}$total = $total + $sumb4tax + $tax;

echo "<br>";
echo "<b>Total</b>: $".$total;
echo "<br>";

if ($total > 750){
    echo "<br>";
    echo "<b>Your credit card has been declined</b>";
    
}else {
    echo "<b>Transaction approved</b>";
}

?>