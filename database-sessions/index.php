<?php
  session_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Cars - CPSC 2030</title>
  </head>
  <body class="container">
    <div>
      <div class="col-md-6 mb-3">
        <h1>Cars - CPSC 2030</h1>
      </div>
      <div class="col-md-6 mb-3">
        <?php
          if(isset($_SESSION['loggedin'])){
            echo "<h3>Welcome, You can now add and delete the data.</h3>";
            echo "<a href= 'logout.php' class = 'btn btn-danger'>Logout</a>";
          }else{
            echo "<a href = 'login.php' class ='btn btn-info'>Login</a>";
            echo "<h3>Login to add and delete the data</h3>";
          }
        ?>
      </div>
    </div>
    <form class="needs-validation" novalidate method="POST" action="">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="invalid-feedback">
              Valid model is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
             <input type="text" class="form-control" id="year" placeholder="" value="" required name="year">
             <div class="invalid-feedback">
              Valid year is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="year">Mileage</label>
             <input type="text" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
             <div class="invalid-feedback">
              Valid mileage is required.
            </div>
        </div>
        <?php
          if(isset($_SESSION['loggedin'])){
            echo "<button class='btn btn-primary btn-lg btn-block' type='submit'>Add car</button>";
          }
        ?>
    </form>

    <table class="table">
        <thead>
          <?php
            if(isset($_SESSION['loggedin'])){
              print
              "<tr>
                  <th scope='col'>ID</th>
                  <th scope='col'>Make</th>
                  <th scope='col'>Model</th>
                  <th scope='col'>Year</th>
                  <th scope='col'>Mileage</th>
                  <th scope='col'>Operation</th>
              </tr>";
            }else{
              print
              "<tr>
                  <th scope='col'>ID</th>
                  <th scope='col'>Make</th>
                  <th scope='col'>Model</th>
                  <th scope='col'>Year</th>
                  <th scope='col'>Mileage</th>
              </tr>";
            }
          ?>
        </thead>
        <tbody>
            <?php
                
                $link = mysqli_connect( 'localhost', 'root', 'Kurobane123' ); 
                mysqli_select_db( $link, 'demo' );
                $results = mysqli_query( $link, 'SELECT * FROM cars' );
                
                if (mysqli_connect_errno()){
                  echo "Failed to connect to MySQL: " . mysqli_connect_error();
                }
                  
                if (!mysqli_query($link,'SELECT * FROM cars')){
                  echo("Error description: " . mysqli_error($link));
                }
                  
                
                
                if ( isset( $_REQUEST["make"] ) ) {
                  $safe_make = mysqli_real_escape_string( $link, $_REQUEST["make"] );
                  $safe_model = mysqli_real_escape_string( $link, $_REQUEST["model"] );
                  $safe_year = mysqli_real_escape_string( $link, $_REQUEST["year"] );
                  $safe_mileage = mysqli_real_escape_string( $link, $_REQUEST["mileage"] );
                  $query = "INSERT INTO cars ( Make, Model,Year,Mileage ) VALUES ( '$safe_make', '$safe_model','$safe_year','$safe_mileage' )";
                
                  if (is_nan($_REQUEST["year"]) or is_nan($_REQUEST["mileage"])){
                    echo "Please enter <b>NUMBERS</b> for year and mileage";
                  }
                
                  mysqli_query( $link, $query );
                  header("Location:index.php");
                
                }
                // process $results

                
                
                
                
                while( $record = mysqli_fetch_assoc( $results ) ) {
                  $ID = $record['ID'];
                	$make = $record['Make'];
                	$model = $record['Model'];
                	$year = $record['Year'];
                	$mileage = $record['Mileage'];
                	
                	
                	if(isset($_SESSION['loggedin'])){
                	  print
                	    "<tr>
                    	<td>$ID</td>
                    	<td>$make</td>
                    	<td>$model</td>
                    	<td>$year</td>
                    	<td>$mileage</td>
                    	<td><a class = 'btn btn-danger' href=delete.php?id=".$record['ID'].">Delete</a></td>
                  	</tr>";
                	}else{
                	print 
                  	"<tr>
                    	<td>$ID</td>
                    	<td>$make</td>
                    	<td>$model</td>
                    	<td>$year</td>
                    	<td>$mileage</td>
                  	</tr>";
                  }
                }
                
              
                
                mysqli_free_result( $results );
                mysqli_close( $link );
            
            ?>
        </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
   
  </body>
</html>
<?php
    

  

?>