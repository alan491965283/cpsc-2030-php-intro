<?php
    session_start();
    $link = mysqli_connect( 'localhost', 'root', 'Kurobane123' ); 
    mysqli_select_db( $link, 'project' );
    if (mysqli_connect_errno()){
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
    if(isset($_POST['comfirm'])){

        $old_password = $_POST['old_password'];
        $new_password = $_POST['new_password'];
        $re_password = $_POST['re_password'];
        
        $database_result = mysqli_query($link,"SELECT password FROM account where username='" . $_SESSION['username'] . "'");
        
        while($record = mysqli_fetch_assoc($database_result)){
            $password = $record['password'];
            
            if($password == $old_password){
                if($password != $new_password){
                    if($new_password == $re_password){
                        $update_query = "Update account SET password = '$new_password' WHERE username = '"  . $_SESSION['username'] . "'";
                        mysqli_query($link,$update_query);
                        $success = true;
                    }else{
                        $no_match = true;
                    }
                }else{
                    $match = true;
                }
            }else{
                $old_error = true;
            }
            
            
        }
        
        
    }
        
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>   
            <main>
                <div class='forms'>
                    <h1 class='subtitle'>Change Password</h1>
                    <form class="form" action="changePW.php" method="POST">

                        
                        <label for="old_password">Old Password:</label>
                        <input type="password" id="old_password" name="old_password" placeholder="Old Password" required>
                        
                        <label for="new_password">New Password:</label>
                        <input type="password" id="new_password" name="new_password" placeholder="New Password" required>
                        
                        <label for="re_password">comfirm New Password:</label>
                        <input type="password" id="re_password" name="re_password" placeholder="Comfirm New Password" required>
                        
                        <input type="submit" value="Comfirm" name="comfirm">
                        <?php
                            if($success){
                                echo "<p style='margin-left:200px;color:green;'>Password Changed! Please wait...</p>";
                                header("refresh:2; url=manage.php");
                            }
                            if($no_match){
                                echo "<p style='margin-left:200px;color:red;'>New password and confirm password doesn't match!</p>";
                                $no_match = false;
                            }
                            if($old_error){
                                echo "<p style='margin-left:200px;color:red;'>Old password is wrong</p>";
                                $old_error = false;
                            }
                            if($match){
                                echo "<p style='margin-left:200px;color:red;'>New password can't be the same as old password</p>";
                                $match = false;
                            }
                        ?>
                    </form>
                </div>
            </main>
        </div>
    </body>
</html>  