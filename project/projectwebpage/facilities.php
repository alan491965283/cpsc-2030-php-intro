<?php
   session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name='description' content='Best Internet Cafe. Facilities gear'>
        <title>Facilities</title>
        <link rel='stylesheet' href='style.css'>
    </head>
    <body>
        <div class="wrapper">
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>
            <main>
                <h1 class='subtitle'>Facilities</h1>
                <div class='facilities clearfix'>     
                    <img src='img/facilities2.jpg' alt='facilities' class='facilities'>
                    <ul>
                        <li>32-inch 144hz refresh rate Monitors for gaming. </li>
                        <li>Comfortable gaming chair.</li>
                        <li>The most comfortable and best sounding closed professional headset on the market.</li>
                        <li>Gaming Keyboard that's 100% anti-ghosting to ensure accurate gameplay.</li>
                        <li>Incredible mouse sensor crushes the competition with a Resolution Accuracy of 99%, so you can land more killing blows with pinpoint precision.</li>
                    </ul>
                </div>
                
                <div class='introVIP'>
                    <h1 class='subtitle'>Not Satisfiy?Check Our VIP Room</h1>
                    <p class='introVIP'>Our VIP rooms are the highest service that we offer,they are private rooms with best gears and beds. You can do whatever you like and we ensure you that nobody will come in and interrupt without your permission. <a href="vip.html">Click here to learn more!</a></p>
                </div>
            </main>
        </div>
    </body>
</html>