<?php
   session_start();
?>
<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta name='description' content='Best Internet Cafe. Vip Room overnight'>
        <title>VIP Rooms</title>
        <link rel='stylesheet' href='style.css'>
    </head>
    <body>
        <div class='wrapper'>
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>
            <main>
                <h1 class='subtitle'>VIP Room</h1>
                <img src='img/viproom.jpg' alt='room' class='room'>
                <p class='VIP clearfix'>If you are looking for a place to play games overnight without destracting, come try out our VIP room! Our VIP rooms are the highest service that we offer,they are private rooms with best gears and beds. Our rooms are 100% soundproof, which means you can do whatever you like in the room and we ensure you that nobody will come in and interrupt without your permission.</p>
                <h1 class='subtitle'>About Our VIP Rooms</h1>
                <div id='aboutVIP'>
                    <dl>
                        <dt>Privacy</dt>
                        <dd>Unlike playing in the lobby, you can do whatever you want in the room without distracting</dd>
                        <dt>Overnight</dt>
                        <dd>You are welcome to stay overnight in our VIP rooms, the room is totally soundproof. There will still be internet even after we close, and we offer you the best gears in the room.</dd>
                        <dt>Clean</dt>
                        <dd>We replace the blanket everytime the previous customer leaves, you won't have to worry about unhygienic problems.</dd>
                        <dt>Hotel Service with Low Price</dt>
                        <dd>We offer you hotel services, such as delivering foods or any other services. The price for our rooms are $10/hr, and notice that discount also works for VIP rooms.</dd>
                        <dt>Even Better equipment</dt>
                        <dd>We offer double monitors in the room, so you can have one monitor to do one thing and the other one to track informations. In our VIP rooms, PS4 and XBox are available as well.</dd>
                    </dl>
                </div>
            </main>
        </div>
    </body>
</html>