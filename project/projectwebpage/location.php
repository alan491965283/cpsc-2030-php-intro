<?php
   session_start();
?>
<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta name='description' content='Best Internet Cafe. Location address'>
        <title>Location</title>
        <link rel='stylesheet' href='style.css'>
    </head>
    <body>
        <div class='wrapper'>
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>
            <main>
                <h1 class="subtitle">Location</h1>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5207.119658342595!2d-123.00496854277058!3d49.265788390164474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548677302bbb89c9%3A0x8cd9fbb5a5ca7b30!2s1969+Willingdon+Ave%2C+Burnaby%2C+BC+V5C+5J3!5e0!3m2!1sen!2sca!4v1553214917075" allowfullscreen class='googlemap'></iframe>
                <p id="introLocat">Our cafe is located at 1969 Willingdon Ave, Burnaby, BC V5C 5J3, and there are many bus stops nearby.</p> 
                <div id='whyNearBy'>
                    <dl>
                        <dt>Resturants</dt>
                        <dd>Our Cafe is surrounded by resturants, therefore our staff would be pleasure to deliver food to you from nearby resturants.</dd>
                        <dt>Supplies</dt>
                        <dd>There is a mall right infront of our Cafe, you won't have to worry about supplies for staying overnight in our VIP room.</dd>
                        <dt>Cash</dt>
                        <dd>TD bank is right beside our Cafe, it's very convenient for you if you want to withdraw money to buy supplies or make payment.</dd>
                    </dl>
                </div>
            </main>
        </div>
    </body>
</html>