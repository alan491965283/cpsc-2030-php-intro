<?php
   session_start();
?>
<!DOCTYPE html>
<html lang='en'>
    <head>
        <meta name='description' content='Best Internet Cafe. contact feedback comments'>
        <title>Contact Us</title>
        <link rel='stylesheet' href='style.css'>
    </head>
    <body>
        <div class='wrapper'>
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>
            <main>
                <h1 class='subtitle'>Contact Us</h1>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5207.119658342595!2d-123.00496854277058!3d49.265788390164474!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x548677302bbb89c9%3A0x8cd9fbb5a5ca7b30!2s1969+Willingdon+Ave%2C+Burnaby%2C+BC+V5C+5J3!5e0!3m2!1sen!2sca!4v1553214917075" allowfullscreen class='googlemap'></iframe>
                <div id='feedback'>
                    <h2>Send us a email, give us a call, or come talk to us for feedback and advice!</h2>
                    <p class='feedbackinfo'>Address: 1969 Willingdon Ave, Burnaby, BC V5C 5J3</p>
                    <p class='feedbackinfo'>Telephone:778-928-0103</p>
                    <p class='feedbackinfo'>Email:bestcafe@gmail.com</p>
                </div>
                <div class='contactform'>
                    <h2>Send us feedback/advice/comments!</h2>
                    <form action="https://formspree.io/bestcafe@gmail.com" method="POST">
                        <label for='name'>*Name:</label>
                        <input type='text' name='name'>
                        <label for='email'>*Email:</label>
                        <input type='text' name='email'>
                        <label for='subject'>Subject</label>
                        <input type='text' name='subject'>
                        <label for='message'>*Message:</label>
                        <br>
                        <textarea id='message'></textarea>
                        <br>
                        <button type='submit' value='submit'>Submit</button>
                    </form>
                </div> 
            </main>
        </div>
    </body>
</html>