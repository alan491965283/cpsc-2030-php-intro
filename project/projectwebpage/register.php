<?php
    session_start();
    
    $link = mysqli_connect( 'localhost', 'root', 'Kurobane123' ); 
    mysqli_select_db( $link, 'project' );
    
    if (mysqli_connect_errno()){
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }
    
    if(isset($_REQUEST['username'])){
        $username = mysqli_real_escape_string( $link, $_REQUEST["username"] );
        $password = mysqli_real_escape_string( $link, $_REQUEST["password"] );
        $email = mysqli_real_escape_string( $link, $_REQUEST["email"] );
        
        $insert = "INSERT INTO account (username,password,email) VALUES ('$username','$password','$email')";
        $query_user = "SELECT * FROM account WHERE username = '$username'";
        $query_email = "SELECT * FROM account WHERE email = '$email'";
        $result_user = mysqli_query($link,$query_user);
        $result_email = mysqli_query($link,$query_email);
        
        if(mysqli_num_rows($result_user)>0){
            $error_user = true;
            
        }else if (mysqli_num_rows($result_email)>0){
            $error_email = true;
            
        }else{
            mysqli_query($link,$insert);
            header("Location:login.php");
        }
        
        
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>   
            <main>
                <h1 class='subtitle'>Registeration</h1>
                <div class='forms'>
                    <form class="form" action="register.php" method="POST">
                        <label for="username">Username</label>
                        <input type="text" id="username" name="username" placeholder="username" required>
                        <?php
                            if($error_user == true){
                                echo "<p style='color:red'>Username already taken, try another one.</p>";
                            }
                        ?>
                        
                        <label for="password">Password</label>
                        <input type="password" id="password" name="password" placeholder="password" required>
                        <label for="email">Email</label>
                        <input type="email" id="email" name="email" placeholder="123@email.com" required>
                        <?php
                            if($error_email == true){
                                echo "<p style='color:red'>Email already taken, try another one.</p>";
                            }
                        ?>
                        <input type="submit" value="Register" name="register">
                    </form>
                </div>
            </main>
        </div>
    </body>
</html>    
