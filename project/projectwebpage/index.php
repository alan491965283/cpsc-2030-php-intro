<?php
   session_start();
?>
<!DOCTYPE html> 
<html lang="en">
    <head>
        
        <meta name='description' content='Best Internet Cafe. Main Page'>
        <title>Best Internet Cafe</title>
        
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="wrapper">
            <header>
                <h1 id="title">Best Internet Cafe</h1>
            </header>
            <div class="navbar">
                <nav>
                    <ul>
                        <li><a href="index.php">Home</a>
                        </li><li><a href="facilities.php">Facilities</a>
                        </li><li><a href="vip.php">VIP Rooms</a>
                        </li><li><a href="location.php">Location</a>
                        </li><li><a href="contactus.php">Contact Us</a>
                        </li><li><a href="manage.php"</li>Manage My Account</a></li>
                    </ul>
                </nav>
            </div>   
            <main>
                <div class="intro">
                    <p>If you are looking for a place to play with friends without distacting, this is the right place for you. We set up suitable environment for you the same way professional players have, and we have staff always ready to offer services, so you don't have to leave during your game for any reason. Please just come give us a try! </p>
                    <img src="img/lobby.jpg" alt="lobby" class="lobbyimg">
                </div>
                    <h1 class="subtitle">Why Choose us?</h1>
                <div id="why">
                    <dl>
                        <dt>Discounts</dt>
                        <dd>If you come with your friends, we will offer 20% discount for you and your friends! Also, if you stay for more than 3 hours, we also offer 10% discount! Note that the discount CAN be stacking!</dd>
                        <dt>Surprise</dt>
                        <dd>We have a mystery present for our new guest!</dd>
                        <dt>Extremely Fast Internet</dt>
                        <dd>We canguarantee the ping will always be stable and low during your game, and you won't disconnect from your game.</dd>
                        <dt>VIP Rooms</dt>
                        <dd>Private rooms with better gears and comfortable bed for you to stay overnight! Well soundproof door and walls making sure you won't get destracted by anyone! Once you're in VIP Rooms you are welcome to stay overnight!</dd>
                        <dt>Premium input&output devices</dt>
                        <dd>32-inch monitors with 144hz offering the best game experiences. High quality mouse and keyboard which increase your game experience to the next level!</dd>
                    </dl>
                </div>
                    <h1 class="subtitle">FAQs</h1>
                <div id="FAQ">
                        <dl>
                            <dt>How do I pay?</dt>
                            <dd>You can basically pay with any ways such as debit, credit, paypal, alipay, wechatpay.</dd>
                            <dt>What can I ask the staff to do for me?</dt>
                            <dd>The stuff can help you extend your available game, purchase food and drinks nearby.</dd>
                            <dt>Are most games available?</dt>
                            <dd>Yes.</dd>
                            <dt>Can I download games if you don't have it?</dt>
                            <dd>Yes.</dd>
                        </dl>
                </div>
                <div class="table">
                    <h1 class="subtitle">Open Hours & Cost</h1>
                        <table>
                            <tr>
                                <th id ="day">Day</th>
                                <th id ="time">Time</th>
                                <th id ="cost">Cost</th>
                            </tr>
                            <tr>
                                <td headers="day">Monday</td>
                                <td headers="time">10am ~ 6am</td>
                                <td headers="cost" rowspan="4">$5/hr</td>
                            </tr>                            
                            <tr>
                                <td headers="day">Tuesday</td>
                                <td headers="time">10am ~ 6am</td>
                            </tr>
                            <tr>
                                <td headers="day">Wednesday</td>
                                <td headers="time">10am ~ 6am</td>
                            </tr>
                            <tr>
                                <td headers="day">Thursday</td>
                                <td headers="time">10am ~ 6am</td>
                            </tr>
                            <tr>
                                <td headers="day">Fri & Sat & Sun</td>
                                <td headers="time">All Day</td>
                                <td headers="cost">$2/hr</td>
                            </tr>
                            <tr>
                                <td headers="day">Mon~Sun</td>
                                <td headers="time">VIP Rooms(Overnight)</td>
                                <td headers="cost">$10/hr</td>
                            </tr>
                        </table>
                </div>
            </main>
        </div>
    </body>
</html>