<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>PHP Intro Lab</title>
  </head>
  <body class="container">
    <h1>PHP Intro Lab</h1>

    <form action="">
        <div class="form-group">
          <label for="word">Word</label>
          <input type="text" name="word" id="word" class="form-control" placeholder="enter word to hightlight">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Highlight</button>
        </div>
    </form>
    
    <?php
       // ini_set('display_errors',1);
        $sonnet = nl2br(" \nShall I compare thee to a summer's day?
        \nThou art more lovely and more temperate:
        \nRough winds do shake the darling buds of May,
        \nAnd summer's lease hath all too short a date:
        \nSometime too hot the eye of heaven shines,
        \nAnd often is his gold complexion dimm'd;
        \nAnd every fair from fair sometime declines,
        \nBy chance, or nature's changing course, untrimm'd;
        \nBut thy eternal summer shall not fade
        \nNor lose possession of that fair thou ow'st;
        \nNor shall Death brag thou wander'st in his shade,
        \nWhen in eternal lines to time thou grow'st;
        \nSo long as men can breathe or eyes can see,
        \nSo long lives this, and this gives life to thee.");
        
        $wordCount = 0;
        $word = "";
        
        if ( isset( $_GET["word"]) ) { //if there is word in the input, word = input
            $word = trim( $_GET["word"]);
            $wordLength = strlen($word);
            $wordcount = substr_count(strtolower($sonnet),strtolower($word),$wordLength); //count word
            $sonnet = str_ireplace($word,'<b>'.$word.'</b>',$sonnet);
        }
    ?>
    <h2>
        <?php print $word ?> appears <?php print $wordcount ?> times in this sonnet.
    </h2>
    <div>
        <?php
            // print the sonnet with highlighting
            print $sonnet;
        ?>
    </div>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    
  </body>
</html>