<?php
    header( "Content-type: application/json");
    
    $link = mysqli_connect( 'localhost', 'root', 'Kurobane123' );
    if ( ! $link ) {
      $error_number = mysqli_connect_errno();
      $error_message = mysqli_connect_error();
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    $dbName = "demo";
    if ( ! mysqli_select_db( $link, $dbName ) ) {
      $error_number = mysqli_errno( $link );
      $error_message = mysqli_error( $link );
      file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
      http_response_code( 500 );
      exit(1);
    }
    
    switch ( $_SERVER['REQUEST_METHOD']) {
        case 'GET':
            $results = mysqli_query( $link, 'select * from cars' );
            
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $shoppinglist[] = $record;
                }
                mysqli_free_result( $results );
                echo json_encode( $shoppinglist );
            }
            break;
        
        case 'POST':
            $safe_make = mysqli_real_escape_string( $link, $_REQUEST["Make"] );
            $safe_model = mysqli_real_escape_string( $link, $_REQUEST["Model"] );
            $safe_year = $_REQUEST["Year"]+0;
            $safe_mileage = $_REQUEST["Mileage"]+0;
            
            $query = "INSERT INTO cars ( Make, Model, Year, Mileage ) VALUES ( '$safe_make', '$safe_model', $safe_year, $safe_mileage )";            
            
            if ( strlen( $safe_make ) <= 0 ||strlen( $safe_make ) > 80 ||is_nan($safe_year) ||is_nan($safe_mileage)) {
                file_put_contents( "/tmp/ajax.log", "Invalid Client Request\n", FILE_APPEND );
                http_response_code( 400 );
                
            }

            
            if ( ! mysqli_query( $link, $query ) ) {
              $error_number = mysqli_errno( $link );
              $error_message = mysqli_error( $link );
              file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
              http_response_code( 500 );
              header("Location:index.php");
            }
            
            
            break;
        
        case 'DELETE':
            $results = mysqli_query( $link, 'select * from cars' );
            if ( ! $results ) {
                $error_number = mysqli_errno( $link );
                $error_message = mysqli_error( $link );
                file_put_contents( "/tmp/ajax.log", "($error_number) $error_message\n", FILE_APPEND );
                http_response_code( 500 );
                exit(1);
            } else {
                $shoppinglist = array();
                while( $record = mysqli_fetch_assoc( $results ) ) {
                    $ID = $record['ID'];
                }
            }
            $query = "DELETE FROM cars WHERE ID ='$ID'";
            mysqli_query($link,$query);
                

    }
