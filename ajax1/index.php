<?php
  session_start();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>AJAX</title>
  </head>
  <body class="container">
    <h1>AJAX</h1>
    <div class="form-group row">
        <div class="col">
          <?php
            if(isset($_SESSION['loggedin'])){
              
              echo "<a href='logout.php' class = 'btn btn-danger data-toggle='modal' data-target='#exampleModal' data-whatever='@mdo'>Logout</a>"; 
              
            }else{
              echo "<button type='button' class='btn btn-primary new_emp' data-toggle='modal' data-target='#exampleModal' data-whatever='@mdo'>Login</button>";
            }
          ?>

        </div>
    </div>
    
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModal" aria-hidden="true">
        <div class="modal-dialog" role='document'>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action='#'>
                        <div class="form-group">
                            <label for="email" class="Email">Email:</label>
                            <input type="email" name = 'email' class="form-control" id="email" value='alan491965283@gmail.com'>
                        </div>
                        <div class="form-group">
                            <label for="password" class="Password">Password:</label>
                            <input type="password" name = 'password' class="form-control" id="password" value='123'>
                        </div>
                        <div class="form-group row d-none" id="errmsg">
                            <div data-dismiss="alert" class = "alert alert-secondary" role="alert">
                                Error
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">Cancel</button>
                    <button class='btn btn-primary' type='button' name='login' id='login'>Login</button>
                </div>
            </div>
        </div>
    </div>
    
    
    <form id="carform" class="needs-validation" novalidate method="POST" action="">
      <div class="row">
        <div class="col-md-6 col-md-3">
          <label for="make">Make</label>
            <input type="text" class="form-control" id="make" placeholder="" value="" required name="make">
            <div class="invalid-feedback">
              Valid make is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="model">Model</label>
            <input type="text" class="form-control" id="model" placeholder="" value="" required name="model">
            <div class="invalid-feedback">
              Valid model is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="year">Year</label>
             <input type="text" class="form-control" id="year" placeholder="" value="" required name="year">
             <div class="invalid-feedback">
              Valid year is required.
            </div>
          </div>
          <div class="col-md-6 mb-3">
            <label for="year">Mileage</label>
             <input type="text" class="form-control" id="mileage" placeholder="" value="" required name="mileage">
             <div class="invalid-feedback">
              Valid mileage is required.
            </div>
        </div>

      <div class="col-md-12">
        <?php
          if(isset($_SESSION['loggedin'])){
            echo "<button class='btn btn-primary btn-lg btn-block' type='submit' name='submit' id='add'>Add car</button>";
          }
        ?>
      </div>
    </form>
    <table class="table">
      <thead>
        <?php
            if(isset($_SESSION['loggedin'])){
              print
              "<tr>
                  <th scope='col'>ID</th>
                  <th scope='col'>Make</th>
                  <th scope='col'>Model</th>
                  <th scope='col'>Year</th>
                  <th scope='col'>Mileage</th>
                  <th scope='col'>Operation</th>
              </tr>";
            }else{
              print
              "<tr>
                  <th scope='col'>ID</th>
                  <th scope='col'>Make</th>
                  <th scope='col'>Model</th>
                  <th scope='col'>Year</th>
                  <th scope='col'>Mileage</th>
              </tr>";
            }
          ?>
      </thead>
      <tbody>
        
      </tbody>
    </table>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="form-validation.js"></script>
  
    <script>
        /* global $ */
        $(document).ready(function(){
          $("#login").on('click',function(){
            var email = $("#email").val();
            var password = $("#password").val();
            
            if(email == "" || password == ""){
              alert("Both fields are required!");
            }else{
              $('#exampleModal').modal('hide');
              $.ajax(
                  {
                    url:'login.php',
                    method:'POST',
                    data:{
                      login:1,
                      Email:email,
                      Password:password
                    },
                    success:function(response){
                      console.log(response);
                    },
                    dataType:'text'
                  }
                );
                
              location.reload();
            }
          });
          $('#add').on('click',function(){
            location.reload();
          })
      });
      </script>
      <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function () {
        'use strict';
        
        window.addEventListener('load', function () {
          var add = document.querySelector('#add'); 
          var table = document.querySelector('tbody');
          var form = document.querySelector('#carform');
          var make = document.querySelector('#make');
          var model = document.querySelector('#model');
          var year = document.querySelector('#year');
          var mileage = document.querySelector('#mileage');
          mileage.focus();

          function addItem( record ) {
            var tr = document.createElement('tr');
            var tdID = document.createElement('td');
            var tdmake = document.createElement('td');
            var tdmodel = document.createElement('td');
            var tdyear = document.createElement('td');
            var tdmileage = document.createElement('td');
            var tdbtn = document.createElement('td');
            
            var tdIDText = document.createElement('span');
            var tdmakeText = document.createElement('span');
            var tdmodelText = document.createElement('span');
            var tdyearText = document.createElement('span');
            var tdmileageText = document.createElement('span');
            var tdBtn = document.createElement('button');
            tdBtn.setAttribute( "class", "btn btn-danger");
            
            tr.appendChild(tdID);
            tdID.appendChild(tdIDText);
            tdIDText.textContent = record.ID;
            
            tr.appendChild(tdmake);
            tdmake.appendChild(tdmakeText);
            tdmakeText.textContent = record.Make;
            
            tr.appendChild(tdmodel);
            tdmodel.appendChild(tdmodelText);
            tdmodelText.textContent = record.Model;
            
            tr.appendChild(tdyear);
            tdyear.appendChild(tdyearText);
            tdyearText.textContent = record.Year;
            
            tr.appendChild(tdmileage);
            tdmileage.appendChild(tdmileageText);
            tdmileageText.textContent = record.Mileage;
            
            if(document.body.contains(add)){
              tr.appendChild(tdbtn);
              tdbtn.appendChild(tdBtn);
              tdBtn.textContent = 'Delete';
            }
            
            table.appendChild(tr);
            tdBtn.onclick = function(e) {
              table.removeChild(tr);
              mileage.focus();
              var request = new XMLHttpRequest();
              request.open("DELETE","shoppinglist.php");
              request.send();
            };
          }

          function handleSubmit(event) {
            if (this.checkValidity() === false) {
              event.preventDefault();
              event.stopPropagation();
              return;
            }
            this.classList.remove('was-validated');
            
            var record = { "Make": make.value, "Model": model.value, "Year": year.value, "Mileage": mileage.value };
            addItem( record );
            
            var formData = new FormData();
            formData.append("Make", make.value );
            formData.append("Model", model.value );
            formData.append("Year", year.value );
            formData.append("Mileage", mileage.value );

            var request = new XMLHttpRequest();
            request.open("POST", "shoppinglist.php");
            request.send(formData);

            make.value = '';
            model.value = '';
            year.value = '';
            mileage.value = '';
            mileage.focus();
            event.preventDefault();
            event.stopPropagation();
            form.classList.remove('was-validated');
          }
          form.addEventListener('submit', handleSubmit );
          
          
          function reqListener () {
            this.response.forEach( function( record ){
              console.log( record );
              addItem( record );
            });
          }
        
          var oReq = new XMLHttpRequest();
          oReq.addEventListener("load", reqListener);
          oReq.responseType = "json";
          oReq.open("GET", "shoppinglist.php");
          oReq.send();
        }, false);
      }());    
      

    </script>  
  </body>
</html>